# About

![slither logo](slither.jpeg)

adsrv/slither project is to promote websites using slither.io

# The biz model

The nickname under which you play represents an advertized domain. The advertiser pays us for the ad and we pay you. 
Nickname assignment and payments are conducted by our tracking software. 
All payments are in USDC on BNC network.

# Install

The steps to install the game and start to earn.

1. Unpack the archive to any folder.
2. Run `main.exe`
3. Press `ENTER` to accept the terms of use. This is required only on the first run of the program.
4. The program will open a tab in your default browser.
5. Connect your Metamask account. This account will receive your rewards in USDC.
6. Copy the nickname the system assigns to you.
7. Press `Open game` to open a new tab with slither.io
8. Paste the assigned nickname.
9. When you reach the leaderboard, the system will automatically transfer you **2$ per minute** in the leaderboard.
10. When playing, keep `main.exe` running. This is a client which tracks your progress and pays you rewards.

**Dashboard**

![dashboard](dash.en.png)

**Watch DEMO**
https://gitlab.com/adsrv/slither/-/blob/main/DEMO.mp4

**Download**
https://gitlab.com/adsrv/slither/-/blob/main/adslither.win10-v1.0.0.zip
